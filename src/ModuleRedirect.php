<?php
namespace TMCms\Modules\Redirect;

use \TMCms\Admin\Structure\Entity\PageRedirectHistoryEntity;
use \TMCms\Admin\Structure\Entity\PageRedirectHistoryEntityRepository;
use \TMCms\HTML\BreadCrumbs;
use \TMCms\HTML\Cms\CmsFormHelper;
use \TMCms\HTML\Cms\CmsTable;
use \TMCms\HTML\Cms\Column\ColumnData;
use \TMCms\HTML\Cms\Column\ColumnDelete;
use \TMCms\HTML\Cms\Column\ColumnEdit;

class ModuleRedirect
{
    public static function requireTableForExternalModule(){
        BreadCrumbs::getInstance()
            ->addAction(__('Add'), '?p='. P .'&do=add&m=redirect')
        ;

        $items = PageRedirectHistoryEntityRepository::getInstance()->setWherePageId('0');

        echo CmsTable::getInstance()
            ->addData($items)
            ->addColumn(ColumnData::getInstance('old_full_url')->href('?p=' . P . '&do=edit&id={%id%}&m=redirect')->enableOrderableColumn())
            ->addColumn(ColumnData::getInstance('new_full_url')->href('?p=' . P . '&do=edit&id={%id%}&m=redirect')->enableOrderableColumn())
            ->addColumn(ColumnEdit::getInstance()->href('?p=' . P . '&do=edit&id={%id%}&m=redirect'))
            ->addColumn(ColumnDelete::getInstance())
        ;
    }

    public static function requireFormForExternalModule($data = NULL){
        $edit_mode = true;
        if (!$data) {
            $data = new PageRedirectHistoryEntity();
            $edit_mode = false;
        }

        return CmsFormHelper::outputForm($data->getDbTableName(), [
            'data' => $data, // Can be simple key-value array or Entity
            'action' => '?p=' . P . '&do=_'.($edit_mode ? 'edit&id='. $data->getId() : 'add') .'&m=redirect', // Url to submit post data, may be omitted for auto "_function" prefix
            'button' => $edit_mode ? 'Update' : 'Add', // Text on submit button
            'cancel' => true, // Show Cancel button

            'fields' => [
                'old_full_url' => [
                ],
                'new_full_url' => [
                ],
            ],
        ]);
    }
}